import 'package:my_shopping_list/service/status.dart';

class ApiResponse<T> {
  Status status;
  T? data;
  String message = "";

  ApiResponse.error(this.message) : status = Status.error;

  ApiResponse.loading(this.message) : status = Status.loading;

  ApiResponse.completed(this.data) : status = Status.completed;

  @override
  String toString() {
    return "Status: $status\tMessage: $message\tData: $data";
  }
}
