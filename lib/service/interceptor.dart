import 'dart:async';

import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:http_interceptor/http_interceptor.dart';
import 'package:my_shopping_list/service/app_exception.dart';

class ConnectionInterceptor implements InterceptorContract {
  final Connectivity _connectivity = Connectivity();

  @override
  Future<RequestData> interceptRequest({required RequestData data}) async {
    debugPrint("Initiated ${data.method.toString()} request: ${data.url}");
    late ConnectivityResult connectivityResult;
    try {
      connectivityResult = await _connectivity.checkConnectivity();
    } on PlatformException catch (e) {
      throw FetchDataException(e.toString());
    }
    if (connectivityResult == ConnectivityResult.none) {
      throw FetchDataException('No internet connection');
    }
    return data;
  }

  @override
  Future<ResponseData> interceptResponse({required ResponseData data}) async {
    debugPrint('Response status: ${data.statusCode}');
    debugPrint('Response Content-Type: ${data.headers!['Content-Type']}');
    return data;
  }
}
