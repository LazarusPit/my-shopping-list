import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:my_shopping_list/model/model.dart';
import 'package:my_shopping_list/serializer/serializer.dart';
import 'package:http_interceptor/http_interceptor.dart';
import 'package:my_shopping_list/service/interceptor.dart';
import 'app_exception.dart';

typedef ItemCreator<S> = S Function();

class ApiHelper<E extends Model, S extends Serializer> {
  http.Client client =
      InterceptedClient.build(interceptors: [ConnectionInterceptor()]);

  final ItemCreator<S> _serializerCreator;
  final String baseUrl = "jsonplaceholder.typicode.com";

  ApiHelper(this._serializerCreator);

  late final serializer = _serializerCreator();

  Uri _parseUrl(String path) => Uri.https(baseUrl, path);

  E _decodeToModel(http.Response response) {
    final utfDecodedBody = utf8.decode(response.bodyBytes);
    final statusCode = response.statusCode;
    switch (statusCode) {
      case 200:
        return serializer.fromJsonString(utfDecodedBody);
      case 400:
        throw BadRequestException(utfDecodedBody);
      case 401:
        throw UnauthorizedException(utfDecodedBody);
      case 403:
        throw ForbiddenException(utfDecodedBody);
      case 500:
      default:
        throw FetchDataException(
            "Error occurred while communicating with the server: "
            "Code $statusCode");
    }
  }

  Future<E> get(String path) async {
    final url = _parseUrl(path);
    final response = await client.get(url);
    return _decodeToModel(response);
  }

  Future<E> delete(String path) async {
    final url = _parseUrl(path);
    final response = await client.delete(url);
    return _decodeToModel(response);
  }

  Future<E> post(E entity, String path) async {
    final url = _parseUrl(path);
    final response = await client.post(url, body: serializer.toJson(entity));
    return _decodeToModel(response);
  }
}
