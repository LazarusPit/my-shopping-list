class AppException implements Exception {
  final String message;
  final String prefix;

  AppException({required this.message, required this.prefix});

  @override
  String toString() => "$prefix$message";
}

class FetchDataException extends AppException {
  FetchDataException([message]) :
        super(prefix: "Error during communication: ", message: message);
}

class BadRequestException extends AppException {
  BadRequestException([message]) :
      super(prefix: "Invalid request: ", message: message);
}

class UnauthorizedException extends AppException {
  UnauthorizedException([message]) :
      super(prefix: "Unauthorized request: ", message: message);
}

class ForbiddenException extends AppException {
  ForbiddenException([message]) :
      super(prefix: "Forbidden request: ", message: message);
}

class InvalidInputException extends AppException {
  InvalidInputException([message]) :
      super(prefix: "Invalid input: ", message: message);
}