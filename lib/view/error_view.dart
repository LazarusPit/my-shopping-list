import 'package:flutter/material.dart';

class ErrorView extends StatelessWidget {
  final String errorMessage;
  final Function onRetryPressed;
  final bool native;

  const ErrorView(
      {Key? key,
      this.errorMessage = "",
      required this.onRetryPressed,
      this.native = false})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
        child: (native)
            ? ErrorWidget(errorMessage)
            : Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(errorMessage,
                      textAlign: TextAlign.center,
                      style: const TextStyle(color: Colors.red, fontSize: 18)),
                  const SizedBox(height: 8),
                  ElevatedButton(
                      onPressed: () => onRetryPressed,
                      child: const Text('Retry'))
                ],
              ));
  }
}
