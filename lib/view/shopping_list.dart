import 'package:flutter/material.dart';
import 'package:english_words/english_words.dart';
import 'package:my_shopping_list/bloc/product_list_bloc.dart';
import 'package:my_shopping_list/model/product.dart';
import 'package:my_shopping_list/model/product_list.dart';
import 'package:my_shopping_list/service/api_response.dart';
import 'package:my_shopping_list/service/status.dart';
import 'package:my_shopping_list/view/error_view.dart';

class ShoppingList extends StatefulWidget {
  const ShoppingList({Key? key}) : super(key: key);

  @override
  _ShoppingListState createState() => _ShoppingListState();
}

class _ShoppingListState extends State<ShoppingList> {
  late ProductListBloc _productListBloc;
  late List<Product> _suggestions;
  final _bought = <Product>{};
  final _biggerFont = const TextStyle(fontSize: 18);

  @override
  void initState() {
    super.initState();
    var id = 0;
    _productListBloc = ProductListBloc();
    _suggestions = generateWordPairs()
        .take(10)
        .map<Product>((word) => Product(id: ++id, name: word.asPascalCase))
        .toList();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('My Shopping List'),
        actions: [
          IconButton(onPressed: _pushSaved, icon: const Icon(Icons.list))
        ],
      ),
      body: RefreshIndicator(
          onRefresh: () => _productListBloc.fetchProductList(),
          child: StreamBuilder<ApiResponse<ProductList>>(
              stream: _productListBloc.productListStream,
              builder: (context, snapshot) {
                if (snapshot.hasData) {
                  final data = snapshot.data!;
                  final status = data.status;
                  return (status == Status.error)
                      ? ErrorView(
                          errorMessage: snapshot.data!.message,
                          onRetryPressed: () =>
                              _productListBloc.fetchProductList())
                      : (status == Status.completed)
                          ? ListView.builder(
                              itemCount: snapshot.data!.data!.items.length,
                              itemBuilder: (context, i) {
                                return _buildSuggestions();
                              })
                          : const Center(child: RefreshProgressIndicator());
                } else {
                  return Container();
                }
              })),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          _addEntry();
        },
        child: const Icon(Icons.add),
      ),
    );
  }

  void _addEntry() {
    Navigator.of(context).push(MaterialPageRoute(builder: (context) {
      return Scaffold(
          appBar: AppBar(
            title: const Text("New Item"),
          ),
          body: const Text("Empty"));
    }));
  }

  void _pushSaved() {
    Navigator.of(context).push(MaterialPageRoute(builder: (context) {
      final tiles = _bought.map((item) {
        return ListTile(title: Text(item.name, style: _biggerFont));
      });
      final divided = tiles.isNotEmpty
          ? ListTile.divideTiles(context: context, tiles: tiles).toList()
          : <Widget>[];

      return Scaffold(
          appBar: AppBar(title: const Text('Saved Suggestions')),
          body: ListView(children: divided));
    }));
  }

  Widget _buildRow(Product item) {
    final alreadyBought = item.bought >= item.wanted;
    return ListTile(
      title: Text(
        item.name,
        style: _biggerFont.apply(
            decoration: alreadyBought ? TextDecoration.lineThrough : null),
      ),
      subtitle: Text(item.description),
      trailing: Icon(
          alreadyBought ? Icons.check_box : Icons.check_box_outline_blank,
          color: alreadyBought ? Colors.red : null,
          semanticLabel: alreadyBought ? 'Remove from bought' : 'Bought'),
      onTap: () {
        setState(() {
          if (alreadyBought) {
            _bought.remove(item);
          } else {
            _bought.add(item);
          }
        });
      },
    );
  }

  Widget _buildSuggestions() {
    return ListView.builder(
      padding: const EdgeInsets.all(16.0),
      itemCount: _suggestions.length * 2,
      itemBuilder: (context, i) {
        if (i.isOdd) return const Divider();
        final index = i ~/ 2;
        return _buildRow(_suggestions[index]);
      },
    );
  }
}
