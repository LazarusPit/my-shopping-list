import 'package:my_shopping_list/service/api_helper.dart';
import 'package:my_shopping_list/model/product_list.dart';
import 'package:my_shopping_list/serializer/product_list_serializer.dart';

class ProductListRepository {
  final ApiHelper<ProductList, ProductListSerializer> _helper = ApiHelper(() => ProductListSerializer());

  Future<ProductList> getAll() async => await _helper.get("posts");
}