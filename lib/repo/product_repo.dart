import 'package:my_shopping_list/service/api_helper.dart';
import 'package:my_shopping_list/model/product.dart';
import 'package:my_shopping_list/serializer/product_serializer.dart';

class ProductRepository {
  final ApiHelper<Product, ProductSerializer> _helper = ApiHelper(() => ProductSerializer());

  Future<Product> get(int index) async  => await _helper.get("posts/$index");
}