import 'package:my_shopping_list/model/product.dart';
import 'package:my_shopping_list/model/product_list.dart';
import 'package:my_shopping_list/serializer/product_serializer.dart';
import 'package:my_shopping_list/serializer/serializer.dart';

class ProductListSerializer extends Serializer<ProductList> {
  @override
  dynamic toJson(ProductList entity) => {
    entity.items
  };

  @override
  ProductList fromJson(json) {
    if (json is! List) {
      return ProductList([ProductSerializer().fromJson(json)]);
    }
    else {
      return ProductList(json.map<Product>((item) {
        return ProductSerializer().fromJson(item);
      }).toList());
  }
  }

}