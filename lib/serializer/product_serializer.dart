import 'package:my_shopping_list/model/product.dart';
import 'package:my_shopping_list/serializer/serializer.dart';

class ProductSerializer extends Serializer<Product> {
  @override
  dynamic toJson(Product entity) => {
    "id": entity.id,
    "name": entity.name,
    "description": entity.description,
    "unit": entity.unit,
    "wanted": entity.wanted,
    "bought": entity.bought
  };

  @override
  Product fromJson(dynamic json) {
    return Product(id: json['id'], name: json['title']);
  }

}