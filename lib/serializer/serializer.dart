import 'dart:convert';

abstract class Serializer<T> {
  dynamic toJson(T entity);
  T fromJson(dynamic json);
  T fromJsonString(String jsonString) {
    final dynamic json = jsonDecode(jsonString);
    return fromJson(json);
  }

  String toJsonString() => jsonEncode(this);
}
