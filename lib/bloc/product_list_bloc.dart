import 'dart:async';

import 'package:my_shopping_list/model/product_list.dart';
import 'package:my_shopping_list/repo/product_list_repo.dart';
import 'package:my_shopping_list/service/api_response.dart';

class ProductListBloc {
  late final ProductListRepository _repo;
  late final StreamController<ApiResponse<ProductList>> _productListController;

  StreamSink<ApiResponse<ProductList>> get productListSink => _productListController.sink;
  Stream<ApiResponse<ProductList>> get productListStream => _productListController.stream;

  ProductListBloc() {
    _repo = ProductListRepository();
    _productListController = StreamController();
    fetchProductList();
  }

  fetchProductList() async {
    productListSink.add(ApiResponse.loading("Fetching product details."));
    try {
      ProductList product = await _repo.getAll();
      productListSink.add(ApiResponse.completed(product));
    } catch (e) {
      productListSink.add(ApiResponse.error(e.toString()));
      print(e.toString());
    }
  }

  dispose() {
    _productListController.close();
  }
}