import 'dart:async';

import 'package:my_shopping_list/model/product.dart';
import 'package:my_shopping_list/repo/product_repo.dart';
import 'package:my_shopping_list/service/api_response.dart';

class ProductBloc {
  late final int selectedProduct;
  late final ProductRepository _repo;
  late final StreamController<ApiResponse<Product>> _productController;

  StreamSink<ApiResponse<Product>> get productSink => _productController.sink;
  Stream<ApiResponse<Product>> get productStream => _productController.stream;

  ProductBloc(this.selectedProduct) {
    _repo = ProductRepository();
    _productController = StreamController();
  }

  fetchProduct() async {
    productSink.add(ApiResponse.loading("Fetching product details."));
    try {
      Product product = await _repo.get(selectedProduct);
      productSink.add(ApiResponse.completed(product));
    } catch (e) {
      productSink.add(ApiResponse.error(e.toString()));
      print(e.toString());
    }
  }

  dispose() {
    _productController.close();
  }
}