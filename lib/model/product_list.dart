import 'package:my_shopping_list/model/model.dart';
import 'package:my_shopping_list/model/product.dart';

class ProductList extends Model {
  late final List<Product> items;

  ProductList(this.items): super(0);
}