import 'package:my_shopping_list/model/model.dart';

class Product extends Model {
  String name = "";
  String description = "";
  int wanted = 0;
  int bought = 0;
  String unit = "";

  Product({required int id, required this.name}) : super (id);
}